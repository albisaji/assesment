import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { StackedBarComponent } from './stacked-bar/stacked-bar.component';
import { DoughnutGraphComponent } from './doughnut-graph/doughnut-graph.component';


const routes: Routes = [
  { path: '', component: DashboardComponent }
];
@NgModule({
  declarations: [
    DashboardComponent,
    StackedBarComponent,
    DoughnutGraphComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],

  exports: [RouterModule]
})
export class DashboardModule { }
