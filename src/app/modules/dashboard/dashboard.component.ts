import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3'
import { STACKED_DATA_1, STACKED_DATA_2 } from 'src/app/config/graph.config';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  stackedBarGraphData1:any={graphData:STACKED_DATA_1,id:'chart1'};
  stackedBarGraphData2:any={graphData:STACKED_DATA_2,id:'chart2'};

  constructor() {

  }

  ngOnInit(): void {
  }

}
