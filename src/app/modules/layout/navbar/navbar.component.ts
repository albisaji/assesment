import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isShowDropdown:boolean=false;
  ngOnInit(): void {
  }
  openDropdown():void{
    this.isShowDropdown=!this.isShowDropdown;
  }

}
